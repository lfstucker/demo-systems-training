# lfstucker-gitlabtraining-cloud

This project template provides an all-in-one configuration of a shared GitLab Omnibus instance, Sidekiq instance, autoscaling GitLab Runner instances, and a Kubernetes cluster for use in training classes.

## Installation Steps

This environment is deployed using GitLab CI in an existing GCP project. See [INSTALL.md](INSTALL.md) for step-by-step build instructions.

## Connecting to the Environment

### GitLab Team Member Access

Each user can visit https://gitlabdemo.cloud (powered by [gitlabdemo-cloud-app](https://gitlab.com/gitlab-com/demo-systems/management-apps/gitlabdemo-cloud-app)) to sign in with Okta and generate credentials using the GitLab API.

### Student Access

TODO

### Root Access

> You must be a member of the `Demo Systems Admins` Google Group and 1Password vault to access the infrastructure.

* GitLab UI Access - [https://lfstucker.gitlabtraining.cloud](https://lfstucker.gitlabtraining.cloud)
    * 1Password Credentials `Omnibus Root - lfstucker.gitlabtraining.cloud`
* GCP Project Console (lstucker-01cf16d9) - [https://console.cloud.google.com/compute/instances?project=lstucker-01cf16d9&authuser=0](https://console.cloud.google.com/compute/instances?project=lstucker-01cf16d9&authuser=0)
* SSH to `lfstuckergitlab-omnibus-instance`
    ```
    gcloud beta compute ssh --zone "us-west1-a" "lfstuckergitlab-omnibus-instance"  --project "lstucker-01cf16d9"
    ```
* SSH to `lfstuckergitlab-runner-manager-instance`
    ```
    `gcloud beta compute ssh --zone "us-west1-a" "lfstuckergitlab-runner-manager-instance"  --project "lstucker-01cf16d9"
    ```
* Connect to `lfstuckergitlab-ci-cluster`
    ```
    gcloud container clusters get-credentials lfstuckergitlab-ci-cluster --region us-west1 --project lstucker-01cf16d9
    ```
    ```
    k9s
    ```

## Operational Tasks

> You must be a member of the `Demo Systems Admins` Google Group and 1Password vault to access the infrastructure.

### GitLab Omnibus Instance

#### Enable Feature Flag

1. Connect to the GitLab Omnibus instance using SSH.

1. Enter the `gitlab-rails console` shell.
    ```
    sudo -i
    gitlab-rails console
    ```

1. Check if the feature flag has been enabled.
    ```
    Feature.enabled?(:my_awesome_feature)
    ```
    ```
    => false
    ```

1. Enable the feature flag.
    ```
    > Feature.enable(:my_awesome_feature)
    ```
    ```
    => nil
    ```

### GitLab Runner Manager

#### Get list of runner instances

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Get a list of runner instances.
    ```
    sudo -i
    docker-machine ls
    ```

#### Update configuration of runners

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

#### Redeploy runner instances

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

1. Update the `IdleCount` value to `0` and save the file.

1. Wait a few minutes for the runners to power off. You can monitor this by trailing the logs.
    ```
    tail -f /var/log/syslog
    ```

1. You can check the status of the machines using `docker-machine ls`.
    ```
    docker-machine ls
    ```

1. If needed, you can delete a specific instance.
    ```
    docker-machine kill {machine-name}
    ```

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

1. Update the `IdleCount` value to `5` (or another value) and save the file.

1. You can monitor the redeployment by trailing the logs.
    ```
    tail -f /var/log/syslog
    ```

### GitLab CI Kubernetes Cluster

See the [k9s key bindings](https://github.com/derailed/k9s#key-bindings) for keyboard usage.

#### View pods for specific namespace

1. Connect to the cluster with `k9s`.

1. Locate the namespace you're looking for. The integer is the GitLab project ID.
    ```
    :namespaces [RETURN]
    ```
    ```
    /12345 [RETURN]
    ```

1. Use up and down arrow keys to highlight the namespace and press `[RETURN]`.

1. See the commands in the top right corner.

#### Delete namespace and clear cluster cache

1. Connect to the cluster with `k9s`.

1. Locate the namespace you're looking for. The integer is the GitLab project ID.
    ```
    :namespaces [RETURN]
    ```
    ```
    /12345 [RETURN]
    ```

1. Use up and down arrow keys to highlight the namespace press `[SPACEBAR]`. Then press `[CTRL] + [D]`. Tab over to `OK` and press `[RETURN]`.

1. In the GitLab UI, navigate to `Admin > Kubernetes` and select the `[prefix]-gitlab-ci-cluster` cluster.

1. Select the `Advanced Settings` tab and click the `Clear cluster cache` button.

1. Re-run the pipeline that had a problem.
