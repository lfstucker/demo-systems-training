# Environment Build Instructions

**Time Estimate:** This can be done in under an hour, however it can take 3 hours if you run into debugging challenges.

## GitLab CI File

Rename the `.gitlab-ci.yml.example` file to `.gitlab-ci.yml` to activate your CI pipelines that will be used throughout the installation process.

## Pre-Requisite Variables to Replace

> **Estimated Time to Complete:** 10 Minutes

This template has placeholders that need to be replaced prior to reading the installation instructions. Use your text editor and project-wide find and replace to replace these capitalized variable keys with the value that matches your environment. This will automatically update all of the files including the `INSTALL.md` instructions with the values specific to your environment.

For Demo Systems deployments, we remove `gitlab` from FQDN alphadash slugs for brevity and to reduce string length to avoid character length limits. Example `ilt-gitlabtraining-cloud` may shortened to `ilt-training-cloud`. This is notated in the list of variables below with `shortened`.

* **Environment FQDN** (`lfstucker.gitlabtraining.cloud`)- The FQDN of the environment that students will be able to access at. In most cases this is a subdomain FQDN. Example `lfstucker.gitlabtraining.cloud`. For GitLab Training purposes, the `*.gitlabtraining.cloud` domains are reserved for environments that are managed by the team and `*.gitlabtraining.net` are ephemeral environments that are specific to one user or a small group of users.
* **Environment Group Prefix** (``) - These environments were designed for the GitLab Demo Systems, however they can be used universally. If this is being deployed as part of the Demo Systems, this value should be `demosys-`. You can choose your own prefix if this is for another purpose, or replace the variable to with a blank value to remove it.
* **Environment Unique Name** (`Logan Stuckers test deployment of the demo systems env`)- A descriptive name for the environment that is used in the description of resources that are created. Example `Customer Success ILT Training Cloud`.
* **Environment Unique Slug** (`lfstucker-training`) - The alphadash slug of the environment that is used for universal naming when the `lstucker-01cf16d9` or `lfstucker-gitlabtraining-cloud` isn't appropriate. If this is being deployed as part of the Demo Systems, this value should be the FQDN of the environment (shortened) that has hyphens (`-`) instead of periods (`.`) and does not have a prefix or suffix, and has not been shortened.
* **Environment Unique Prefix** (`lfstucker`) - An optional prefix that easily distinguishes this environment from others. This is prefixed on most resources that are created. If this is being deployed as part of the Demo Systems, this value should be the subdomain DNS value. For sandbox purposes, this could be your user handle. Example `ilt-` or `jsmith-`.
* **GCP Machine Type for Omnibus** (`e2-standard-16`) - See the GCP documentation for [e2 machine types](https://cloud.google.com/compute/docs/machine-types#e2_machine_types). The CPU and memory that you need will depend on the number concurrent active users. Based on lessons learned with the Demo Systems, we recommend `e2-standard-16` (16 CPU / 64GB) for up to 50 concurrent users, and `e2-standard-32` (32 CPU / 128GB) for up to 500 users. These are not GitLab product official recommendations since this caters to the use case of dozens or hundreds of students clicking on the same button at the same time. Please see [GitLab reference architecture documentation](https://docs.gitlab.com/ee/administration/reference_architectures/) for traditional use case sizing recommendations.
* **GCP Project Name** (`lstucker-01cf16d9`) - The name of the GCP project where this environment is being deployed in. To allow universal use, the schema of your GCP project name will vary so this is explicitely defined without prefixes or suffixes. If this is being deployed as part of the Demo Systems, this value should start with `demosys-` and be followed with the alphadash slug of the FQDN (shortened). Example. `demosys-ilt-training-cloud`
* **GCP Region Zone** (`us-west1-a`) - See the GCP documentation for [available regions and zones](https://cloud.google.com/compute/docs/regions-zones#available) that has a list of features and machine types supported in each zone. If you are using our preferred or alternative preferred regions listed above, you can use the recommended zone listed below. There is no problem with using another zone in the same region.

    | GCP Region        | GCP Region Zone (preferred) |
    |-------------------|-----------------------------|
    | `us-central1`     | `us-central1-c`             |
    | `europe-west1`    | `europe-west1-b`            |
    | `asia-southeast1` | `asia-southeast1-c`         |

* **GCP Region** (`us-west1`) - See the GCP documentation for [available regions and zones](https://cloud.google.com/compute/docs/regions-zones#available). At GitLab, we use regions that have the lowest cost and are centrally located relative to the intended user audience. The following regions have the lowest compute cost:

    | GCP Region                      | Location                           | n1-standard-2 Hourly Price |
    |---------------------------------|------------------------------------|----------------------------|
    | `us-central1` (preferred)       | Council Bluffs, Iowa, USA          | $0.0950                    |
    | `us-east1`                      | Moncks Corner, South Carolina, USA | $0.0950                    |
    | `us-west1`                      | The Dalles, Oregon, USA            | $0.0950                    |
    | `europe-north1`                 | Hamina, Finland                    | $0.1046                    |
    | `europe-west1` (alternative)    | St. Ghislain, Belgium              | $0.1046                    |
    | `europe-west4`                  | Eemshaven, Netherlands             | $0.1046                    |
    | `asia-south1`                   | Mumbai, India                      | $0.1141                    |
    | `asia-southeast1` (alternative) | Jurong West, Singapore             | $0.1172                    |

* **GCP Storage Bucket Name** (`lstucker-01cf16d9-terraform-state`)- The name of the Google Cloud Storage (GCS) bucket that has been created for managing the Terraform State. If this is being deployed as part of the Demo Systems, this value should start wth `demosys-` and be followed with the alphadash slug of the FQDN (shortened), followed by a suffix with `terraform-state`. Example `demosys-ilt-training-cloud-terraform-state`.

* **GitLab Project Slug** (`lfstucker-gitlabtraining-cloud`) - The alphadash slug of the GitLab project (repository) that this template has been imported to. This usually uses the FQDN of the environment (shortened) that has hyphens (`-`) instead of periods (`.`) and may have a suffix. Example `ilt.gitlabtraining.cloud` is `ilt-gitlabtraining-cloud-iac`.

* **GitLab Root Email Address** (`lstucker@gitlab.com`) - The email address of the root user for the GitLab Omnibus instance. You can use `username+alias@domain.tld` on most email providers to add a unique email address that forwards to your `username@domain.tld` to distinguish where email notifications are coming from. If this is being deployed as part of the Demo Systems, this value should use the syntax `demo-systems-admin+xxx-omnibus@gitlab.com` where `xxx` is the subdomain value. Example `demo-systems-admin+ilt-omnibus@gitlab.com`.

* **GitLab API Provisioner Email Address** (`lstucker@gitlab.com`) - The email address of the API user for the GitLab Omnibus instance. You can use `username+alias@domain.tld` on most email providers to add a unique email address that forwards to your `username@domain.tld` to distinguish where email notifications are coming from. If this is being deployed as part of the Demo Systems, this value should use the syntax `demo-systems-admin+xxx-api-provisioner@gitlab.com` where `xxx` is the subdomain value. Example `demo-systems-admin+ilt-api-provisioner@gitlab.com`.

## Infrastructure Standards Labels

1. Open the `terraform/terraform.tfvars.json` file and update the `labels` array with the appropriate values based on the [GitLab Infrastructure Standards Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) handbook documentation. You may find it helpful to look at other environment repositories to see the values used for those and adapt accordingly.

## 1Password Vault

> **Estimated Time to Complete:** 2 Minutes

> All records should be created in the `Demo Systems Admins` 1Password vault.

1. Create a new 1Password Login record named `Omnibus Root - lfstucker.gitlabtraining.cloud` with the username `root` and a 24-character generated password.

1. Create a new 1Password password record named `Ansible Vault Password - lfstucker-training` and generate a 32-character password.
<!-- TODO Add api_user -->

## Ansible Vault

> **Estimated Time to Complete:** 5 Minutes

1. Open your text editor and open the `ansible/vault.example.yml` file.

1. Open a Terminal and navigate to the `ansible` directory of this GitLab project.
    ```
    cd ~/Sites/lfstucker-gitlabtraining-cloud/ansible
    ```

1. Create a new Ansible vault.
    ```
    ansible-vault create vault.yml

    Password: (copy/paste from 1Password)
    ```

1. Copy and paste the contents of the `vault.example.yml` file into the Terminal window.

1. Update each of the variable keys with the data for the environment. All of this information should be stored in 1Password.

## SSH Key Generation

> **Estimated Time to Complete:** 5 Minutes

> These instructions are for first time configuration of the GCP project. THese do not need to be run when configuring a new developer machine to run this Ansible.

1. On your local machine, navigate to your SSH directory.
    ```
    cd ~/.ssh
    ```

1. Generate a new SSH key. When prompted, save the file as `lstucker-01cf16d9-ansible` instead of `id_rsa`.
    ```
    ssh-keygen -t rsa -b 4096 -C ansible
    ```

1. Create a new 1Password secure note record named `lstucker-01cf16d9 SSH Key (ansible)`.
    ```
    ssh ansible@{instance_name}.lfstucker.gitlabtraining.cloud

    Public Key lstucker-01cf16d9.pub

    ssh-rsa XXXX== ansible

    Private Key lstucker-01cf16d9

    -----BEGIN OPENSSH PRIVATE KEY-----
    XXXX
    -----END OPENSSH PRIVATE KEY-----
    ```

## SSH Key Configuration on Local Machine

> **Estimated Time to Complete:** 2 Minutes

1. If you did not generate the SSH key on your machine, you need to copy it from the 1Password Vault secure note named `lstucker-01cf16d9 SSH Key (ansible)`.
    ```
    nano ~/.ssh/lstucker-01cf16d9-ansible

    (copy and paste private key)
    ```
    ```
    nano ~/.ssh/lstucker-01cf16d9-ansible.pub

    (copy and paste public key)
    ```

1. Update the permissions of the SSH key.
    ```
    chmod 600 ~/.ssh/lstucker-01cf16d9-ansible*
    ```

1. Update your local SSH configuration file to use the new SSH key to connect to any hosts with the subdomain.
    ```
    nano ~/.ssh/config
    ```
    ```
    Host *.lfstucker.gitlabtraining.cloud
        User ansible
        IdentityFile ~/.ssh/lstucker-01cf16d9-ansible
    ```

## Google Project Initial Configuration

> **Estimated Time to Complete:** 20 Minutes

> These instructions are for first time configuration of the GCP project that this environment will reside in. These do not need to be run when configuring a new developer machine to use this Terraform configuration.

1. Create a GCP project named `lstucker-01cf16d9`.

1. Navigate to **Compute Engine** and enable the `Compute Engine API` service.

1. Navigate to **Kubernetes Engine** and enable the `Kubernetes Engine API` service.

1. Navigate to **Network services > Cloud DNS** and enable the `Cloud DNS API` service.

1. Create an IAM service account named `terraform-ci` with description `GitLab CI service account for Terraform CI deployments`.

1. Grant `Owner` and `Kubernetes Engine Admin` roles to the service account.

1. Generate a new key for the `terraform-ci` service account. Save the JSON into the Demo Systems Admins 1Password vault with the name `lstucker-01cf16d9 GCP Service Account - terraform-ci`.

1. Create an IAM service account named `gitlab-runner-manager` with description `Service account for GitLab Runner Manager`.

1. Grant `Compute Admin`, `Compute Instance Admin (v1)`, and `Service Account User` roles to the service account.

1. Generate a new key for the `gitlab-runner-manager` service account. Save the JSON into the Demo Systems Admins 1Password vault with the name `lstucker-01cf16d9 GCP Service Account - gitlab-runner-manager`.

1. Add a CI variable to the GitLab project.
    * Name: `GOOGLE_APPLICATION_CREDENTIALS`
    * Type: `File`
    * Value: (copy and paste JSON from terraform-ci service account key file)
    * Environment Scope: `All`
    * Protect Variable Flag: `unchecked` (the credentials are needed on branches with MRs for `terraform plan`)
    * Mask Variable Flag: `unchecked` (masked variables cause problems with Terraform and Ansible. The project is private and least privilege already.)

1. Navigate to **Cloud Storage** and create a bucket named `us-west1`.
    * Location Type: `Dual-region`
    * Location: `nam4`
    * Storage Class: `Standard`
    * Access Control: `Uniform`
    * Encryption: `Google managed encryption key`

1. Navigate to **Compute Engine > Settings > Meta Data** and select the `SSH Keys` tab.

1. Add a new SSH key and paste the public key of the Ansible SSH key that you created. You will need to rename the end of the key to remove the full name and only have `ansible` after the `==`.

## Terraform Deployment

> **Estimated Time to Complete:** 30 Minutes (10 minute configuration, 20 minute pipeline wait time)

1. Rename the `.gitlab-ci.yml.example` file to `.gitlab-ci.yml`. This will activate your CI/CD pipelines that are used for deploying the environment with Terraform now that your configuration has been updated with your environment settings.

1. Navigate to the GitLab project and create a new Merge Request (MR) to add your changes in. You can name the merge request anything you like (Example: `Initial configuration`).

1. Commit all of your changes to the repository.

1. Merge your changes into the `master` branch.

1. Navigate to **CI/CD > Pipelines** and click on the latest pipeline. The pipelines on the `master` branch will have the Deploy and Destroy stages available.

1. If the `Validate and Plan` job in the `Dry run` stage was successful, then click on the play button for the `Everything` job in the `Deploy` stage to trigger the manual job to start.

1. It will take up to 20 minutes for all of your Terraform resources to deploy. It is normal for the cluster to take a long time to provision while the Kubernetes services start.

1. After the pipeline job has succeeded, press `[CMD]+[F]` or `[CTRL]+[F]` to open your browser search bar and type in `name_servers`. Locate the 4 DNS name servers that GCP has created the managed DNS zone in. This will look like `ns-cloud-x1.googledomains.com` and the `x` will be an alphabetical character (a-f usually). Copy and paste the 4 name servers into a temporary text file to use in the next step.

## Root Zone DNS Configuration

> The Terraform that you just ran created a new DNS zone for the subdomain. Before that subdomain can be resolved (able to ping resources in this subdomain), you need to update the root domain DNS records to delegate authority for the subdomain to the name server that the new subzone has been created in.

1. Navigate to the [gitlab-com/demo-systems/environments/dns-zones](https://gitlab.com/gitlab-com/demo-systems/environments/dns-zones) GitLab group and locate the root zone for this environment.

1. Create an issue and merge request titled `Add subzone for lfstucker.gitlabtraining.cloud`.

1. Modify the `terraform.tfvars.json` file to add a new subzone (Example: `yyy`) with the name servers that you copied in the previous step. Ensure that each nameserver ends with a period (Example: `ns-cloud-d1.googledomains.com.`) and you do not have a trailing comma at the end of the array or a lint error will occur when the pipeline job runs.

    ```
    {
    "gcp_project": "demosys-dns-zones",
    "dns_zone_fqdn": "gitlabtraining.cloud",
    "subzones": {
        "xxx": [
            "ns-cloud-a1.googledomains.com.",
            "ns-cloud-a2.googledomains.com.",
            "ns-cloud-a3.googledomains.com.",
            "ns-cloud-a4.googledomains.com."
        ],
        "yyy": [
            "ns-cloud-d1.googledomains.com.",
            "ns-cloud-d2.googledomains.com.",
            "ns-cloud-d3.googledomains.com.",
            "ns-cloud-d4.googledomains.com."
        ]
    },
    ```

1. Commit your changes to the repository and watch the `Validate and Plan` job to ensure there are no errors and check that the changes will add a new name server record to the DNS zone.

1. Merge the changes to the `master` branch.

1. Navigate to CI/CD > Pipelines and select the latest pipeline that is running on the `master` branch. Click on the play button for the `Everything` job in the `Deploy` stage to trigger the manual job to start.

1. It may take several minutes to resolve the domain name, however it should resolve before you proceed to the next step.

    ```
    ping lfstucker.gitlabtraining.cloud

    PING lfstucker.gitlabtraining.cloud (104.154.223.52): 56 data bytes
    64 bytes from 104.154.223.52: icmp_seq=0 ttl=59 time=78.151 ms
    64 bytes from 104.154.223.52: icmp_seq=1 ttl=59 time=56.203 ms
    ```
    ```
    ping lfstucker.gitlabtraining.cloud
    ping: cannot resolve lfstucker.gitlabtraining.cloud: Unknown host

    # Clear DNS cache on Mac
    sudo killall -HUP mDNSResponder
    ```
    ```
    dig lfstucker.gitlabtraining.cloud

    ; <<>> DiG 9.10.6 <<>> lfstucker.gitlabtraining.cloud
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39263
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 512
    ;; QUESTION SECTION:
    ;lfstucker.gitlabtraining.cloud.	IN	A

    ;; ANSWER SECTION:
    lfstucker.gitlabtraining.cloud. 299 IN	A	104.154.223.52

    ;; Query time: 264 msec
    ;; SERVER: 192.168.86.1#53(192.168.86.1)
    ;; WHEN: Mon May 17 15:09:19 PDT 2021
    ;; MSG SIZE  rcvd: 74
    ```

## Ansible Configuration

> **Estimated Time to Complete:** 10 Minutes (3 minute configuration, 7 minute progress bar wait time)

1. Create a new file in `ansible/keys/vault.pass` as an empty text file. Copy and paste the vault password from 1Password `Ansible Vault Password - lfstucker-gitlabtraining-cloud`.

1. Duplicate the file `ansible.cfg.example-local` to `ansible.cfg`.

1. Install dependencies for Python environment
    ```
    cd ~/Sites/lfstucker-gitlabtraining-cloud/ansible
    pipenv install --dev
    ```

1. Use the pipenv shell environment to ensure you're using the correct Python environment configuration and version
    ```
    pipenv shell
    ```

1. Install the roles based on the definitions in requirements file
    ```
    (ansible) > ansible-galaxy install -r requirements.yml
    ```

1. Run the playbook to install GitLab on the instance
    ```
    (ansible) > ansible-playbook playbook-deploy-omnibus-instance.yml --vault-password-file=keys/vault.pass --inventory=hosts/hosts.yml
    ```

## Kubernetes Configuration

### Obtain the Cluster CA Certificate

> **Estimated Time to Complete:** 2 minutes

1. Create a new file in your preferred text editor so we can copy and paste values into it for temporary reference.

1. Copy and paste your cluster name (at the top of the cluster dashboard) into the text file (Ex. `lfstuckergitlab-ci-cluster`).

1. In your browser on the cluster dashboard in the GCP console, locate and click on the **Show credentials** link next to the Endpoint (IP address value).

1. In the _Cluster credentials_ pop-up modal window, **copy and paste each of the values to your text editor file**. For the value for the Cluster CA certificate, be sure to include the hyphen lines for `BEGIN CERTIFICATE` and `END CERTIFICATE`.

1. Click the **Close** button in the bottom right corner.

### Configure `gitlab-admin` service account

> **Estimated Time to Complete:** 5 minutes

> These instructions have been customized from the [GitLab documentation](https://lfstucker.gitlabtraining.cloud/help/user/project/clusters/add_remove_clusters#add-existing-cluster) to show the specific instructions for the demo systems infrastructure.

1. On your cluster dashboard in the GCP console, locate and click on the **Connect** button in the top page navigation (below the search bar in the blue top navigation).

1. In the _Connect to the cluster_ pop-up modal, copy and paste the `gcloud` command into your Terminal.

    ```
    gcloud container clusters get-credentials lfstuckergitlab-ci-cluster --region us-west1 --project lstucker-01cf16d9
    ```

    ```
    Fetching cluster endpoint and auth data.
    kubeconfig entry generated for lfstuckergitlab-ci-cluster.
    ```

    > If you do not have `kubectl` and `gcloud` command-line tools, you can to install them from here ([kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-with-homebrew-on-macos), [gcloud](https://cloud.google.com/sdk/docs/downloads-versioned-archives#installation_instructions)).

1. **Use the following command** to get the API URL and **copy/paste the outputted URL to your text file**.

    ```
    kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
    ```
    ```
    https://x.x.x.x
    ```

    > This will return an IP address that is the same as the endpoint IP that you can see in the GCP Console Web UI with an `https://` prefix. Keep in mind that this URL does not provide a web UI for your cluster and is only used for API purposes.

1. **Use the following command** to create the service account.

    ```
    kubectl create sa gitlab-admin -n kube-system
    kubectl create clusterrolebinding gitlab-admin --serviceaccount=kube-system:gitlab-admin --clusterrole=cluster-admin
    ```
   You will receive a confirmation.
    ```
    serviceaccount "gitlab-admin" created
    clusterrolebinding "gitlab-admin" created
    ```

1. **Use the following command** to get the API token for the `gitlab-admin` service account and copy/paste the value of the `token` to your text editor.

    ```
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep token:
    ```
    ```
    token:      XXXXXXXXXXX
    ```

1. You have successfully obtained the credentials needed to connect your cluster from GitLab.

### Activate shell completion for kubectl commands

If you have not already, you should enable shell completion to allow you to tab and see available options for `kubectl` commands.

```
kubectl completion --help
```

### Install the Ingress custom helm chart

> **Estimated Time to Complete:** 5 minutes

1. Connect to the cluster using the `gcloud` command which gets credentials for `kubectl` commands.
    ```
    gcloud config set project lstucker-01cf16d9
    gcloud container clusters get-credentials lfstuckergitlab-ci-cluster --region=us-west1
    ```
1. Create a namespace for GitLab charts that are customized.
    ```
    kubectl create ns gitlab-custom-charts
    ```
1. Install the community nginx ingress helm chart using customizations in the `charts/ingress-nginx/override.yaml` file.
    ```
    # Ensure that you are in the GitLab project directory
    # cd ~/Sites/lfstucker-gitlabtraining-cloud

    helm upgrade --install -f charts/ingress-nginx/override.yaml ci-ingress ingress-nginx/ingress-nginx --namespace=gitlab-custom-charts
    ```
1. Wait a few moments for the ingress to start and get the external IP address of the ingress. You can run this command multiple times to get an updated status.
    ```
    kubectl get svc --namespace gitlab-custom-charts
    ```
    ```
    NAME                                            TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
    ci-ingress-ingress-nginx-controller             LoadBalancer   10.131.120.129   <pending>     80:32294/TCP,443:30464/TCP   40s
    ```
    ```
    NAME                                            TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)                      AGE
    ci-ingress-ingress-nginx-controller             LoadBalancer   10.131.120.129   1.2.3.4         80:32294/TCP,443:30464/TCP   75s
    ```

1. Open the `terraform/terraform.tfvars.json` file and copy and paste the external IP to the `gitlab_ci_cluster_ingress.external_ip` value.

1. In the `terraform/terraform.tfvars.json` file, change the value of `gitlab_ci_cluster_ingress.enabled` from `false` to `true`.
    ```
    "gitlab_ci_cluster_ingress" : {
        "enabled" : "true",
        "external_ip" : "1.2.3.4"
    },
    ```

1. Create a new merge request for the GitLab project titled `Update CI Cluster Ingress External IP` and merge.

1. Open the GitLab project and navigate to CI/CD pipelines. Select the latest pipeline. Run the `Cluster Ingress DNS` job in the `Deploy` stage.

### Generate Wildcard SSL Certificate with Certbot for Cluster Subdomains

> **Estimated Time to Complete:** 20 minutes

> You can install certbot on any machine using package management, however Mac OS brew package (`brew install certbot`) may give you errors so a Linux host (`apt install certbot`) is recommended.

1. Connect to the bastion host that has `certbot` installed.

1. Generate a certificate generation request.
    ```
    certbot certonly -d *.ci.lfstucker.gitlabtraining.cloud -d ci.lfstucker.gitlabtraining.cloud --manual
    ```

1. Copy and paste the DNS TXT record instructions to your temporary text editor file. **Do not press enter or make any changes to this terminal window while you perform additional steps separately.**
    ```
    Please deploy a DNS TXT record under the name
    _acme-challenge.ci.lfstucker.gitlabtraining.cloud with the following value:

    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    Before continuing, verify the record is deployed.
    ```

1. Open the `terraform/terraform.tfvars.json` file and copy and paste the TXT record value to the `gitlab_ci_cluster_letsencrypt_txt.value` value.

1. In the `terraform/terraform.tfvars.json` file, change the value of `gitlab_ci_cluster_letsencrypt_txt.enabled` from `false` to `true`.
    ```
    "gitlab_ci_cluster_letsencrypt_txt" : {
        "enabled" : "true",
        "value" : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    },
    ```

1. Create a new merge request for the GitLab project titled `Update CI Cluster Certbot Verification TXT DNS Record` and merge.

1. Open the GitLab project and navigate to CI/CD pipelines. Select the latest pipeline. Run the `Cluster Certbot DNS` job in the `Deploy` stage.

1. After the Terraform job has run and the new DNS record has been applied, wait for 60 seconds to be safe (for DNS propogation) and then return to the terminal window where you ran the `certbot` command.

1. Press `Enter` for the next step of the verification process.
    ```
    Create a file containing just this data:
    {string1}.{string2}

    And make it available on your web server at this URL:
    http://ci.lfstucker.gitlabtraining.cloud/.well-known/acme-challenge/{string1}
    ```

1. Open the `charts/certbot-verification/kubernetes/configmap.yaml` file and replace the existing `data` value with the value shown in the terminal output from the previous step. Note that the URL value after `acme-challenge/` is the same as the string before the `.` seperator. This is the key for the variable and the value is the concatenated value including the string before and after the `.`.
    ```
    data:
      {string1}: {string1}.{string2}
    ```

1. Deploy the helm chart for the certbot verification. This makes a lightweight web server virtual host that serves the key and value at the specified URL for verification.
    ```
    cd charts/certbot-verification
    kubectl apply -f certbot
    ```

1. After the Kubernetes helm chart has deployed, return to the terminal window where you ran the `certbot` command.

1. Press `Enter` for the next step of the verification process.

1. After the verification is complete, you will see a confirmation message and a path to the SSL Certificate keys.
    ```
    Waiting for verification...
    Cleaning up challenges

    IMPORTANT NOTES:
     - Congratulations! Your certificate and chain have been saved at:
       /etc/letsencrypt/live/ci.lfstucker.gitlabtraining.cloud/fullchain.pem
       Your key file has been saved at:
       /etc/letsencrypt/live/ci.lfstucker.gitlabtraining.cloud/privkey.pem
       Your cert will expire on 2021-08-10. To obtain a new or tweaked
       version of this certificate in the future, simply run certbot
       again. To non-interactively renew *all* of your certificates, run
       "certbot renew"
     - If you like Certbot, please consider supporting our work by:

       Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
       Donating to EFF:                    https://eff.org/donate-le
    ```

1. Create a new 1Password secure note named `SSL Wildcard Certificate - ci.lfstucker.gitlabtraining.cloud`.

1. Use `cat` to get the contents of the the `fullchain.pem`. Copy and paste the output into the 1Password secure note.
    ```
    /etc/letsencrypt/live/ci.lfstucker.gitlabtraining.cloud/fullchain.pem
    ```

1. Use `cat` to get the contents of the `privkey.pem`. Copy and paste the output into the 1Password secure note.
    ```
    /etc/letsencrypt/live/ci.lfstucker.gitlabtraining.cloud/privkey.pem
    ```

1. You can close the certbot Terminal window.

### Copy SSL keys to Cluster for Ingress SSL Termination

> **Estimated Time to Complete:** 5 minutes

1. If you do not have the SSL keys on your local machine, you will need to create temporary files for them to use with `kubectl`. You can create them in any secure directory, the `~/.ssh` is just a personal preference.
    ```
    nano ~/.ssh/ci-lfstucker-training-fullchain.pem

    (copy and paste 1Password contents)
    ```
    ```
    nano ~/.ssh/ci-lfstucker-training-privkey.pem

    (copy and paste 1Password contents)
    ```
2. Create a new Kubernetes secret with the TLS certificate.
    ```
    cd ~/.ssh
    kubectl create secret tls wildcard-cert --cert=ci-lfstucker-training-fullchain.pem --key=ci-lfstucker-training-privkey.pem --namespace=gitlab-custom-charts
    ```
    ```
    secret/wildcard-cert created
    ```

1. Delete the existing `ingress` replicas (pods) using the following command. They will be immediately rebuilt using the configuration with the SSL certificate. You can also do this from `k9s` if the GUI is more comfortable.
    ```
    kubectl get pods -n gitlab-custom-charts --no-headers=true | awk '/ingress/{print $1}' | xargs kubectl delete -n gitlab-custom-charts pod
    ```

## GitLab Omnibus Configuration

1. Sign in to the Omnibus instance with the `root` user account.
    ```
    https://lfstucker.gitlabtraining.cloud
    ```

1. Navigate to [Settings > General > Sign-up Restrictions](https://lfstucker.gitlabtraining.cloud/admin/application_settings/general) and uncheck the `Sign-up Enabled` checkbox. Scroll down to save your changes.

1. Create a new group named `Sample Project Templates` and set the slug to `sample-project-templates`. Set the visibility to `Public`.

1. Navigate to [Settings > Templates > Custom project templates](https://lfstucker.gitlabtraining.cloud/admin/application_settings/templates) and choose the `Sample Project Templates` from the dropdown menu. Click Save Changes.

1. Navigate to [Settings > CI/CD > Variables](https://lfstucker.gitlabtraining.cloud/admin/application_settings/ci_cd) and create the following variables. These provide overrides for legacy versions, bugs, and unnecessary resources for cost savings.
    * `AUTO_DEVOPS_POSTGRES_CHANNEL = 2`
    * `POSTGRES_ENABLED = false`
    * `POSTGRES_VERSION = 11.7.0`
    * `TEST_DISABLED = true`

1. Navigate to [Settings > Kubernetes](https://lfstucker.gitlabtraining.cloud/admin/clusters). Click the `Integrate with a cluster certificate` button.

1. Select the `Connect existing cluster` tab. Use the following details to add the cluster.
    * Kubernetes cluster name: `lfstuckergitlab-ci-cluster`
    * Environment scope: `*`
    * API URL: `https://x.x.x.x` (copy and paste)
    * CA Certificate: (copy and paste with line breaks)
    * Service Token: (copy and paste)
    * RBAC-enabled cluster: (checked)
    * GitLab-managed cluster: (checked)
    * Namespace per environment: (unchecked)

1. After the cluster has been added, set the `Base domain` to `ci.lfstucker.gitlabtraining.cloud`.

1. Do not install any of the 1-click managed applications. We use custom helm charts.

1. Navigate to [Admin > Overview > Runners](https://lfstucker.gitlabtraining.cloud/admin/runners) and copy and paste the runner registration token into your text editor.

1. Navigate to [Admin > Users](https://lfstucker.gitlabtraining.cloud/admin/users) and create a new user account.
    * Account Name: `Logan Stuckers test deployment of the demo systems env API Provisioner`
    * Username: `api-provisioner`
    * Email: `lstucker@gitlab.com`

1. Impersonate the newly created user account.

1. In the top right corner, click the user's avatar and choose `Preferences` from the drop down menu.

1. In the left sidebar, select `Access Tokens` and create a new token.
    * Name: `laravel-app`
    * Expires at: (blank/never)
    * Scope: `api`, `sudo`

1. Create a new 1Password secure note and copy the account details and the newly generated token into the secure note.

## Add Omnibus instance to Laravel database

> These steps must be performed by a Demo Systems Admin.

1. Connect to the Laravel application server.
    ```
    gcloud beta compute ssh --zone "us-central1-c" "laravel-app"  --project "demosys-app-demo-cloud"
    ```
1. Elevate to the root user account.
    ```
    sudo -i
    ```
1. Navigate to the Laravel application directory.
    ```
    cd /srv/www/gitlabdemo.cloud/current
    ```
1. Use `php artisan` CLI command to get a list of available commands if you're not familiar with this application.
    ```
    php artisan
    ```

    > Look for the `auth` and `environment` commands to see what commands are specifically built for this application.

1. Use `php artisan environment-instance:create` to add the new instance to the database. Follow the prompts.
    * Tenant: `default`
    * Display Name: `Logan Stuckers test deployment of the demo systems env Omnibus Instance`
    * Slug: `lfstucker-training`
    * Description: `This is a shared Omnibus instance for lfstucker students with ephemeral access. Instructors can generate credentials for admin access.`
    * Type: `gitlab`
    * URL (users): `https://lfstucker.gitlabtraining.cloud`
    * URL (api): `https://lfstucker.gitlabtraining.cloud`
    * Username: (blank)
    * Token: (copy/paste from 1Password)

1. Use `php artisan environment-instance:connection-test (short_id)` to verify that the API credentials are valid.
    ```
    php artisan environment-instance:connection-test a1b2c3d4
    ```

1. You can close the terminal connection.

1. You can now access this instance by signing into [https://gitlabtraining.cloud](https://gitlabtraining.cloud), locating the instance and clicking the `Generate Credentials` button.

## GitLab Runner Manager Docker Machine Configuration

1. Connect to the Runner Manager instance.
    ```
    gcloud beta compute ssh --zone "us-west1-a" "lfstuckergitlab-runner-manager-instance" --project "lstucker-01cf16d9"
    ```

1. Elevate to the root user.
    ```
    sudo -i
    ```

1. Add the GitLab repository for package management
    ```
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    ```

1. Install GitLab Runner
    ```
    apt install gitlab-runner -y
    ```

1. Install Docker Machine (Reference: https://docs.docker.com/machine/install-machine/)
    ```
    base=https://github.com/docker/machine/releases/download/v0.16.2 &&
    curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
    sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
    chmod +x /usr/local/bin/docker-machine
    ```

1. Verify Docker Machine Version
    ```
    docker-machine version
    ```

1. Add GCP credentials file by copying the contents from the `gitlab-runner-manager` serice account in 1Password vault.
    ```
    nano /etc/gcp_service_account.json
    ```

1. Set the permissions on the service account to `640` (read and write by root user, read by gitlab-runner group).
    ```
    chmod 640 /etc/gcp_service_account.json
    chown root:gitlab-runner /etc/gcp_service_account.json
    ```

1. Set the environment variables. (Reference: https://docs.docker.com/machine/drivers/aws/#environment-variables)
    ```
    export GOOGLE_APPLICATION_CREDENTIALS=/etc/gcp_service_account.json
    export GOOGLE_PREEMPTIBLE=true
    ```

1. Create first machine to establish connectivity
    ```
    docker-machine create --driver google --google-project lstucker-01cf16d9 docker-machine-test-1
    ```

1. Check the machine status and destroy when you're done.
    ```
    docker-machine ls
    docker-machine kill docker-machine-test-1
    ```

1. Edit the daemon service to add the environment variable for the service account.
    ```
    nano /etc/systemd/system/gitlab-runner.service
    ```
    ```
    # Add the Environment line below ExecStart
    Environment="GOOGLE_APPLICATION_CREDENTIALS=/etc/gcp_service_account.json"
    ```
    ```
    [Unit]
    Description=GitLab Runner
    After=syslog.target network.target
    ConditionFileIsExecutable=/usr/bin/gitlab-runner

    [Service]
    StartLimitInterval=5
    StartLimitBurst=10
    ExecStart=/usr/bin/gitlab-runner "run" "--working-directory" "/home/gitlab-runner" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--user" "gitlab-runner"
    Environment="GOOGLE_APPLICATION_CREDENTIALS=/etc/gcp_service_account.json"

    Restart=always
    RestartSec=120

    [Install]
    WantedBy=multi-user.target
    ```

1. Restart the `gitlab-runner` service.
    ```
    systemctl daemon-reload
    service gitlab-runner restart
    ```

1. Register the GitLab runner. Update the registration token with the value from your text file.
    ```
    gitlab-runner register --non-interactive --description lfstuckergitlab-runner-manager --docker-image registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:latest --executor docker+machine --url https://lfstucker.gitlabtraining.cloud --registration-token XXXXXXXXX
    ```
    ```
    Registering runner... succeeded                     runner=XXXXXXXX
    ```

1. Edit the GitLab Runner configuration with the configuration below. Ensure that proper indentation is maintained. This can be tweaked if needed.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

    * Update the `concurrent` value from `1` to `500`.
    ```
    concurrent = 500
    check_interval = 0
    ```

    * Add the `limit = 500` line to the `[[runners]]` section above the `url` line.
    ```
    [[runners]]
      name = "lfstuckergitlab-runner-manager"
      limit = 500
      url = "https://lfstucker.gitlabtraining.cloud"
      ```

    * Change `privileged` from `false` to `true` in the `[runners.docker]` section.
    ```
    [runners.docker]
      tls_verify = false
      image = "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:latest"
      privileged = true
    ```

    * Replace the `[runners.machine]` section with the following.
    ```
    [runners.machine]
      IdleCount = 5
      IdleTime = 600
      MaxGrowthRate = 20
      MaxBuilds = 10
      MachineDriver = "google"
      MachineName = "%s"
      MachineOptions = [
        "google-project=lstucker-01cf16d9",
        "google-zone=us-west1-a",
        "google-preemptible=true",
        "google-disk-size=50",
        #@see https://developers.google.com/identity/protocols/googlescopes
        "google-scopes=https://www.googleapis.com/auth/cloud-platform,https://www.googleapis.com/auth/compute",
      ]
      OffPeakTimezone = ""
      OffPeakIdleCount = 0
      OffPeakIdleTime = 0
      # [second] [minute] [hour] [day of month] [month] [day of week] [year]
      # Burst Capacity 2021-03-17 13:00 to 16:00 UTC
    #    [[runners.machine.autoscaling]]
    #      Periods = ["* * 13-16 17 3 * 2021"]
    #      IdleCount = 200
    #      IdleTime = 600
    #      Timezone = "UTC"
    ```

1. Tail the `syslog` to watch the configuration and deployment of runner instances.

1. To troubleshoot the runners, you can set the `IdleCount` value to `0` to get a clean slate and use `docker-machine ls` to monitor the deployed machines. The errors and timeouts are known behavior of GCP APIs that we have not figured out the root cause of yet.

> NOTE: These steps for the runner manager involved extensive debugging. These steps are at risk of being incomplete or inconsistent, specifically with errors relating to not able to find the service account credentials or that the GCP project does not exist. It may require additional configuration of the `GOOGLE_APPLICATION_CREDENTIALS` environment variable.
